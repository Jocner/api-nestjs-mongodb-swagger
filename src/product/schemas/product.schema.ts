import { Schema, ObjectId } from "mongoose";

const mongoObjectId = function () {
    var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
        return (Math.random() * 16 | 0).toString(16);
    }).toLowerCase();
};



export const ProductSchema = new Schema({
    _id : {
        type: String,
        default: mongoObjectId
    },
    // _id : {
    //    type: Schema.Types.String,
    //    index: true,
    //    required: true,
    //    auto: true 
    // },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imageURL: {
        type: String,
    },
    price: {
        type: Number,
        required: true  
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});