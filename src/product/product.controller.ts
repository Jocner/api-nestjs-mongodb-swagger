import { Controller, Post, Get, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';
import { CreateProductDTO } from './dto/product.dto';

import { ProductService } from './product.service';

@Controller('product')
export class ProductController {

    constructor(private productService: ProductService) {}

    @Post('/create')
    async createPost(@Res() res, @Body() createProductDTO: CreateProductDTO ) {
        const product = await this.productService.createProduct(createProductDTO);
        //console.log(createProductDTO);
        return res.status(HttpStatus.OK).json({
            msg: 'producto creado con exito!!!',
            product
        });
    }

    @Get('/')
    async getProducts(@Res() res) {
        const products = await this.productService.getProducts();

        return res.status(HttpStatus.OK).json({
            products
        });
    }

    @Get('/:productID')
    async getProduct(@Res() res, @Param('productID') productID) {
        const product = await this.productService.getProduct(productID);
        
        if(!product) throw new NotFoundException('product not exists');

        return res.status(HttpStatus.OK).json(product);
    }

    @Delete('/delete')
    async deleteProduct(@Res() res, @Query('productID') productID ) {
        const productDelete = await this.productService.deleteProduct(productID);

        if(!productDelete) throw new NotFoundException('product not exists');

        return res.status(HttpStatus.OK).json({
            msg: 'product delete success!!!',
            productDelete
        });
    }

    @Put('/update')
    async updateProduct(@Res() res, @Body() createProductDTO: CreateProductDTO,  @Query('productID') productID) {
        const updatedProduct = await this.productService.updateProduct(productID, createProductDTO);  

        if(!updatedProduct) throw new NotFoundException('product not exists');

        return res.status(HttpStatus.OK).json({
            msg: 'Product update success!!!',
            updatedProduct  
        });
    }

}
